import historicEventsRoutes from "./historic-events";

const routerManager = (app) => {
    app.get("/", (req, res, next) => {
        res.json({ message: "🏦 Hello Hackathon!!! 💰💰💰" });
    });

    app.use("/historic-events", historicEventsRoutes);
};

export default routerManager;
