const Sequelize = require("sequelize");
const chalk = require("chalk");

import config from "../database/config";

const { database, username, host, password } = config.development;

const log = console.log;

const connection = new Sequelize(database, username, password, {
    host: host,
    dialect: "mariadb",
});

connection
    .authenticate()
    .then(() => log(chalk.bgGreen("Connected to MariaDB  ✅")))
    .catch((err) => console.log("DB ERROR => ", err));

module.exports = connection;
