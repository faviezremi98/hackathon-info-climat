import isObject from "lodash/isObject";
import isArray from "lodash/isArray";
import map from "lodash/map";

export default (data, parser) => {
    if (isArray(data)) return map(data, parser);

    if (isObject) return parser(data);

    return data;
};
