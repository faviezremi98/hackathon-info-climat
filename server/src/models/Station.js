import { DataTypes, Model } from "sequelize";

class Stations extends Model {
    static init(sequelize) {
        super.init(
            {
                id: {
                    type: DataTypes.STRING,
                    allowNull: false,
                    primaryKey: true,
                },
                libelle: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                latitude: {
                    type: DataTypes.DOUBLE,
                    allowNull: true,
                },
                longitude: {
                    type: DataTypes.DOUBLE,
                    allowNull: true,
                },
                altitude: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                pays: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
            },
            {
                sequelize,
                modelName: "Stations",
                tableName: "stations",
                timestamps: false,
            }
        );
    }

    static associate(models) {}
}

export default Stations;
