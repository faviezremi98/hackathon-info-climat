import { DataTypes, Model } from "sequelize";

class HistoricNormales extends Model {
    static init(sequelize) {
        super.init(
            {
                geoid: {
                    type: DataTypes.BIGINT,
                    allowNull: false,
                },
                mois: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },

                tx: {
                    type: DataTypes.FLOAT,
                    allowNull: true,
                },

                tn: {
                    type: DataTypes.FLOAT,
                    allowNull: true,
                },

                precip: {
                    type: DataTypes.FLOAT,
                    allowNull: true,
                },
                altitude_ref: {
                    type: DataTypes.FLOAT,
                    allowNull: true,
                },
                nom_ref: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                dept_ref: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                distance: {
                    type: DataTypes.FLOAT,
                    allowNull: true,
                },

                wmo_ref: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
            },
            {
                sequelize,
                modelName: "HistoricNormales",
                tableName: "historic_normales",
                timestamps: false,
            }
        );
    }

    static associate(models) {}
}

export default HistoricNormales;
