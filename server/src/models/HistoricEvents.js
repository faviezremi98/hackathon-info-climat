import { DataTypes, Model } from "sequelize";

class HistoricEvents extends Model {
    static init(sequelize) {
        super.init(
            {
                nom: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                localisation: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },

                importance: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },

                type_cyclone: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },

                has_image_cyclone: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true,
                },

                date_deb: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                date_fin: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },
                duree: {
                    type: DataTypes.INTEGER, // DataTypes.BOOLEAN,bcs TINYINT
                    allowNull: true,
                },
                type: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                description: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                short_desc: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                sources: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },

                id_compte: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                valeur_max: {
                    type: DataTypes.FLOAT,
                    allowNull: true,
                },
                bs_link: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                gen_cartes: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true,
                },

                why: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                tableau_croise: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
                tableau_croise_cyclone: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },

                hits: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },

                notes: {
                    type: DataTypes.TEXT,
                    allowNull: true,
                },
            },
            {
                sequelize,
                modelName: "HistoricEvents",
                tableName: "historic_events",
                timestamps: false,
            }
        );
    }

    static associate(models) {}
}

export default HistoricEvents;
