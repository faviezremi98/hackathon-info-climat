import { DataTypes, Model } from "sequelize";

class ClimatologieJournaliere extends Model {
    static init(sequelize) {
        super.init(
            {
                id_station: {
                    type: DataTypes.STRING,
                    allowNull: false,
                },
                jour: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                mois: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                annee: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                tn: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                tx: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                rr: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                ens: {
                    type: DataTypes.DECIMAL,
                    allowNull: true,
                },
            },
            {
                sequelize,
                modelName: "ClimatologieJournaliere",
                tableName: "climatologie_journaliere",
                timestamps: false,
            }
        );
    }

    static associate(models) {}
}

export default ClimatologieJournaliere;
