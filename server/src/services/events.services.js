import sequelize from "../lib/sequelize";
import isEmpty from 'lodash/isEmpty';
const { QueryTypes } = require('sequelize');

async function getByLocationWithFiltersData (req) {
  let importance, type, codeRegion, date, id;

  if(!isEmpty(req.query)) {
    ({ importance, type, date, codeRegion, id } = req.query);
  } else {
    importance = 5, date = 2020;
  }

  const query = `select distinct he.id, he.nom, he.type, hv.lieu, ci.latitude, ci.longitude, DATE_FORMAT(hv.date , "%Y-%m-%d") as date
                 from historic_events he
                          inner join historic_values hv on he.id = hv.id_historic
                          inner join city_info ci on ci.nom_commune = hv.lieu
                 WHERE hv.lieu is not null
                 ${ date ? `and SUBSTRING(DATE_FORMAT(date_deb, '%Y-%m-%d'), 1, 4) = '${date}'` : '' }
                   ${importance ? ` and importance = ${importance}`: '' } ${type ? ` and he.type IN (${type})` : ''}
                   ${codeRegion ? ` and (${codeRegion}) IN (he.localisation)` : ''}
                   ${id ? ` and he.id = ${id}` : ''};
                 order by date;`;

  const result =  await sequelize.query(query, {
    plain: false,
    raw: true,
    type: QueryTypes.SELECT
  });

  return result;
}

async function getByLocationWithFiltersSearch (req) {
  let importance, type, date, codeRegion, id;

  if(!isEmpty(req.query)) {
    ({ importance, type, date, codeRegion, id } = req.query);
  }

  const query = `select distinct he.id, he.nom, he.type
                 from historic_events he
                          inner join historic_values hv on he.id = hv.id_historic
                          inner join city_info ci on ci.nom_commune = hv.lieu
                 WHERE hv.lieu is not null
                 ${ date ? `and SUBSTRING(DATE_FORMAT(date_deb, '%Y-%m-%d'), 1, 4) = '${date}'` : '' }
                   ${importance ? ` and importance = ${importance}`: '' } ${type ? ` and he.type IN (${type})` : ''}
                   ${codeRegion ? ` and (${codeRegion}) IN (he.localisation)` : ''}
                   ${id ? ` and he.id = ${id}` : ''}`;

  const result =  await sequelize.query(query, {
    plain: false,
    raw: true,
    type: QueryTypes.SELECT
  });

  return result;
}

async function getMinAndMaxDate (req) {
  let importance, type, date, codeRegion, id;
  console.log(req.query);
  if(!isEmpty(req.query)) {
    ({ importance, type, date, codeRegion, id } = req.query);
  } else {
    importance = 3;
  }

  const query = `select distinct DATE_FORMAT(MIN(hv.date), "%Y-%m-%d") as min, DATE_FORMAT(MAX(hv.date), "%Y-%m-%d") as max
                 from historic_events he
                          inner join historic_values hv on he.id = hv.id_historic
                          inner join city_info ci on ci.nom_commune = hv.lieu
                 WHERE 
                 ${ date ? `SUBSTRING(DATE_FORMAT(date_deb, '%Y-%m-%d'), 1, 4) = '${date}'` : '' }
                   importance = ${importance} ${type ? ` and he.type = ${type}` : ''}
                   ${codeRegion ? ` and (${codeRegion}) IN (he.localisation)` : ''}
                   ${id ? ` and he.id = ${id}` : ''}
                 order by min`;

  const result = await sequelize.query(query, {
    plain: false,
    raw: true,
    type: QueryTypes.SELECT
  });

  console.log(result);

  return result;
}

export { getByLocationWithFiltersSearch, getByLocationWithFiltersData, getMinAndMaxDate };