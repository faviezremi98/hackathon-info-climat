/* eslint-disable import/order */
import React, { useState } from 'react';

import FilterButton from './FilterButton';
import Chips from './Chips';

import wording from 'src/utils/wording.json';
import {
  Accordion as MuiAccordion,
  AccordionDetails as MuiAccordionDetails,
  AccordionSummary as MuiAccordionSummary,
  Button,
} from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { withStyles } from '@material-ui/styles';

const Accordion = withStyles({
  root: {
    border: 'none',
    marginBottom: -1,
    minHeight: 56,
    padding: 0,
    boxShadow: 'none',

    'MuiButtonBase-root': {
      boxShadow: 'none',
      border: 'none',
    },
  },

  content: {
    boxShadow: 'none',
  },
  expanded: {},
})(MuiAccordion);

const AccordionDetails = withStyles({
  root: {
    padding: 0,
  },
})(MuiAccordionDetails);

const AccordionSummary = withStyles({
  root: {
    border: 'none',
    marginBottom: -1,
    minHeight: 56,
    boxShadow: 'none',
    padding: 0,

    '&$expanded': {
      minHeight: 56,
      border: 'none',
    },
  },

  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const Filters = ({ data, handleSetPayload }) => {
  const [openAdvanced, setOpenAdvanced] = useState(false);

  const handleAdvancedBtn = () => {
    setOpenAdvanced((val) => !val);
  };

  const filters = [
    {
      title: "Type d'événement",
      type: 'type',
      stateValues: data.type.map((o) => o.value),
      items: wording.eventTypes,
      handleChange: (value) => {
        handleSetPayload('type', value);
      },
    },

    {
      title: "Lieux d'événement",
      type: 'codeRegion',
      stateValues: data.codeRegion.map((o) => o.value),
      items: wording.placeEvent,
      handleChange: (value) => {
        handleSetPayload('codeRegion', value);
      },
    },

    {
      title: "L'importance de l'événement",
      type: 'importance',
      stateValues: data.importance.map((o) => o.value),
      items: wording.importances,
      handleChange: (value) => {
        handleSetPayload('importance', value);
      },
    },
  ];

  const advancedFilters = [
    {
      title: "Lieux d'événement",
      type: 'durations',
      stateValues: data.codeRegion.map((o) => o.value),
      items: wording.eventDuration,
      handleChange: (value) => {
        handleSetPayload('durations', value);
      },
    },
  ];

  const handleChipDelete = (type, value) => {
    handleSetPayload(type, value);
  };

  return (
    <>
      <Chips data={data} handleChipDelete={handleChipDelete} />

      {filters.map(({ items, stateValues, title, type, handleChange }) => (
        <FilterButton
          title={title}
          type={type}
          stateValues={stateValues}
          items={items}
          handleChange={handleChange}
        />
      ))}

      <Accordion square expanded={openAdvanced} onChange={handleAdvancedBtn}>
        <AccordionSummary
          aria-controls="panel1d-content"
          id="panel1d-header"
          onClick={handleAdvancedBtn}
        >
          <Button
            color="secondary"
            variant="outlined"
            startIcon={!openAdvanced ? <AddIcon /> : <RemoveIcon />}
            onClick={handleAdvancedBtn}
          >
            RECHERCHE AVANCÉE
          </Button>
        </AccordionSummary>
        <AccordionDetails>
          {advancedFilters.map(
            ({ items, stateValues, title, type, handleChange }) => (
              <FilterButton
                title={title}
                type={type}
                stateValues={stateValues}
                items={items}
                handleChange={handleChange}
              />
            )
          )}
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default Filters;
