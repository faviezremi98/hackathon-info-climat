/* eslint-disable function-paren-newline */
import { Box, Chip, makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
  chip: {
    margin: 3,
  },
});

const Chips = ({ data, handleChipDelete }) => {
  const classes = useStyles();

  const renderChips = () =>
    Object.keys(data).map((type) =>
      data[type].map((obj) => (
        <Chip
          label={obj.label}
          onDelete={() => handleChipDelete(type, obj)}
          color="secondary"
          variant="outlined"
          className={classes.chip}
        />
      ))
    );

  return (
    <Box pt={2} pb={2}>
      {renderChips()}
    </Box>
  );
};

export default Chips;
