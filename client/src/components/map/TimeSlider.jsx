/* eslint-disable max-len */
import React, { useMemo, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { KeyboardDatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Slider from './Slider';

const useStyles = makeStyles({
  grid: {
    padding: 0,
  },
  card: {
    minWidth: 275,
    position: 'absolute',
    zIndex: 1000,
    bottom: 50,
    width: '70%',
    borderRadius: '12px',
    marginLeft: 'auto',
    marginRight: 'auto',
    left: 0,
    right: 0,
    backgroundColor: 'white',
    padding: 0,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
  },
  pos: {
    marginBottom: 12,
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
  },
});

const getDates = (startDate, stopDate) => {
  const dateArray = [];
  let currentDate = moment(startDate);
  stopDate = moment(stopDate);
  while (currentDate <= stopDate) {
    dateArray.push(moment(currentDate).format('YYYY-MM-DD'));
    currentDate = moment(currentDate).add(1, 'days');
  }
  return dateArray;
};

export default function TimeSlider({ minAndMaxDate }) {
  const [minDate, setDateMin] = useState(new Date(minAndMaxDate.min));
  const [maxDate, setDateMax] = useState(new Date(minAndMaxDate.max));
  const classes = useStyles();
  const getDateBetween = useMemo(
    () => getDates(minAndMaxDate.min, minAndMaxDate.max),
    [minAndMaxDate.min, minAndMaxDate.max]
  );

  const handleDateMin = (dateMin) => {
    setDateMin(new Date(dateMin.min));
  };

  const updateDatePickerBySlider = (valSlider) => {
    const [min, max] = valSlider;
    setDateMin(getDateBetween[min - 1]);
    setDateMax(getDateBetween[max]);
  };

  const slider = {
    label: 'Time lapse',
    min: 1,
    max: getDateBetween.length - 1,
    valueMin: 1,
    valueMax: getDateBetween.length - 1,
    marks: [
      {
        value: 1,
        label: minAndMaxDate.min,
      },
      {
        value: getDateBetween.length,
        label: minAndMaxDate.max,
      },
    ],
  };

  return (
    <Grid
      container
      justify="center"
      spacing={2}
      className={`${classes.grid} ${classes.card}`}
    >
      <Grid key={0} md={3} item className={`${classes.grid} ${classes.center}`}>
        <KeyboardDatePicker
          clearable
          format="dd/MM/yyyy"
          placeholder="dd/MM/yyyy"
          value={minDate}
          margin="normal"
          onChange={(date) => handleDateMin(date)}
        />
      </Grid>
      <Grid key={1} md={6} item className={classes.grid}>
        <Slider
          updateDatePickerBySlider={updateDatePickerBySlider}
          slider={slider}
          key={1}
          dateArray={getDateBetween}
        />
      </Grid>
      <Grid key={2} md={3} item className={`${classes.grid} ${classes.center}`}>
        <KeyboardDatePicker
          clearable
          format="dd/MM/yyyy"
          placeholder="dd/MM/yyyy"
          value={maxDate}
          margin="normal"
          onChange={(date) => setDateMax(date)}
        />
      </Grid>
    </Grid>
  );
}
