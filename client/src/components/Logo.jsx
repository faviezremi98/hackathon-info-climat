import React from 'react';

const Logo = ({ color = 'white', ...rest }) => (
  <img alt="Logo" src={`/static/logo-${color}.svg`} {...rest} />
);

export default Logo;
