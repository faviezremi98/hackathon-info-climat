import React from 'react';
import { Box, makeStyles, Typography, Avatar } from '@material-ui/core';
import WidgetContainer from '../../WidgetContainer';

const useStyles = makeStyles((theme) => ({
  avatar: {
    marginRight: '4px'
  },
  textTwitter: {
    fontSize: 20,
    color: theme.palette.text.primary,
    lineHeight: '1.1em',
    marginBottom: '15px'
  },
  logoInternet: {
    marginLeft: '5px',
    width: '10px',
    height: '10px'
  }
}));

const SocialNetwork = () => {
  const classes = useStyles();

  const data = {
    label: 'infoClimat',
    filePath: '/static/logo-info-climat.png',
    dateUpload: '1j',
    description: 'Rendez-vous du 16 au 21 juillet au Barboux, dans le Doubs (25), pour notre traditionnelle Rencontre Météo. Tout en respectant les gestes barrières',
    contentPicturePath: '/static/social-media-post-img.jpg',
  };

  return (
    <WidgetContainer title="Nos Réseaux Sociaux">
      <Box margin={2} display="flex" flexDirection="row" alignItems="center" marginBottom={0}>
        <Box display="flex" flexDirection="row" alignItems="center" marginRight={6}>
          <Avatar margin={2} src={data.filePath} className={classes.avatar} />

          <Box display="flex" flexDirection="column">
            <p>{data.label}</p>
            <p>{data.dateUpload} -<img className={classes.logoInternet} alt="logo-web" src="/static/logo-web.png" /></p>
          </Box>

        </Box>
      </Box>
      <Typography>
        <Box margin={4} display="flex" flexDirection="column" marginLeft={4}>

          <span className={classes.textTwitter}>{data.description}</span>

          <img alt="social-media-post-img" src={data.contentPicturePath} />

        </Box>
      </Typography>
    </WidgetContainer>
  );
};

export default SocialNetwork;
