import React from 'react';
import { Box, makeStyles, Typography } from '@material-ui/core';
import WidgetContainer from '../../WidgetContainer';

const useStyles = makeStyles((theme) => ({
  img: {
    width: '25px',
    height: '25px',
    filter: 'invert(100%) sepia(8%) saturate(1%) hue-rotate(274deg) brightness(107%) contrast(101%)',
    borderRadius: '30px !important',
  },
  date: {
    borderRadius: '80%',
    padding: '13px',
    backgroundColor: '#AA00FF',
    top: '10px !important'
  },

  text: {
    fontSize: 15,
    color: theme.palette.text.primary,
    lineHeight: '1.1em',
  },
}));

const DidYouKnow = () => {
  const classes = useStyles();

  const data = [{
    description: 'Le nouveau record absolu de température observée en France est donc officiellement de 46,0 °C et date du 28 juin 2019, à Vérargues',
  }, {
    description: 'La France se classe ainsi au 6e rang des pays européens ayant connu la température la plus élevée.'
  }, {
    description: 'Lors des grands froids la qualité de l\'air peut avoir un indice Atmo de 10/10 à cause des particules en suspension. '
  }];

  return (
    <WidgetContainer title="Le saviez vous">
      {data.map((value, key) => (
        <Box key={key} margin={2} display="flex" flexDirection="row" alignItems="center" marginBottom={0}>
          <Box className={classes.date} marginRight={2}>
            <img className={classes.img} alt="webcam" src="/static/light-bulb.svg" />
          </Box>
          <Typography className={classes.text}>
            {value.description}
          </Typography>
        </Box>
      ))};

    </WidgetContainer>
  );
};

export default DidYouKnow;
