import React from 'react';
import moment from 'moment';
import { Box, capitalize, makeStyles, Typography } from '@material-ui/core';
import WidgetContainer from '../../WidgetContainer';

const useStyles = makeStyles((theme) => ({
  img: {
    objectFit: 'cover',
    flexShrink: 0,
    width: '100%',
    height: '100%',
  },

  date: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '15px 25px',
    backgroundColor: theme.palette.secondary.main,
    borderRadius: '10px',
    color: '#fff',
    width: 'fit-content',
    fontSize: 18,
    marginRight: theme.spacing(4),
  },

  text: {
    fontSize: 20,
    color: theme.palette.text.primary,
  },
}));

const Event = () => {
  const classes = useStyles();

  const data = {
    date: '2021-05-16',
    description: 'Marche pour le climat',
  };

  const { date, month } = (() => {
    const m = moment(data.date);

    return {
      date: m.format('Do'),
      month: m.format('MMM'),
    };
  })();

  return (
    <WidgetContainer title="Événements">
      <Box display="flex" flexDirection="row" alignItems="center">
        <Box className={classes.date}>
          <p> {date} </p>
          <p> {capitalize(month)} </p>
        </Box>

        <Typography className={classes.text}>{data.description}</Typography>
      </Box>
    </WidgetContainer>
  );
};

export default Event;
