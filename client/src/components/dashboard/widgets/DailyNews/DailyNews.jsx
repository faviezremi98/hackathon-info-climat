import React from 'react';
import { Box, makeStyles, Typography } from '@material-ui/core';
import WidgetContainer from '../../WidgetContainer';

const useStyles = makeStyles((theme) => ({
  img: {
    width: '100%',
    height: '100%',
    borderRadius: '0% 0% 5% 5%',
  },
  text: {
    fontSize: 20,
    color: theme.palette.text.primary,
    lineHeight: '1.1em',
  },
  buttonDailyNews: {
    color: 'white',
    border: 'none',
    width: '60%',
    height: '33px',
    backgroundColor: theme.palette.secondary.main,
    borderRadius: '6px 6px 6px 6px '
  }
}));

const DailyNews = () => {
  const classes = useStyles();

  const data = {
    description: 'Gel dévastateur dans le vignoble français: le réchauffement suspect numéro un !',
    filePath: '/static/Info-du-jour-gel-dévastateur.jpg'
  };

  return (

    <WidgetContainer title="Info du jour">

      <Box marginBottom={0}>
        <img className={classes.img} alt="webcam" src={data.filePath} />
      </Box>
      <Box margin={3} display="flex" textAlign="center">
        <Typography className={classes.text}>
          {data.description}
        </Typography>
      </Box>
      <Box margin={4} display="flex" justifyContent="center" marginBottom={5}>
        <button className={classes.buttonDailyNews} type="button"> {'DÉCOUVRIR L\'ARTICLE '}</button>
      </Box>

    </WidgetContainer>
  );
};

export default DailyNews;
