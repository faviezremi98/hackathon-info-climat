/* eslint-disable react/no-danger */
import { Box, makeStyles, Typography } from '@material-ui/core';
import React, { useCallback, useEffect } from 'react';
import moment from 'moment';
import { useDispatch, useSelector } from 'src/store';
import isEmpty from 'lodash/isEmpty';
import { getThisDayEvents } from 'src/slices/home';
import wording from 'src/utils/wording.json';
import WidgetContainer from '../../WidgetContainer';

const useStyles = makeStyles(() => ({
  title: {
    fontWeight: 600,
    margin: '10px 0px',
    fontSize: '20px',
  },
}));
const OnThisDay = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { thisDayEvents, loading } = useSelector((state) => state.home);
  const handleGetEvents = useCallback(() => {
    dispatch(getThisDayEvents());
  }, [dispatch]);

  useEffect(() => {
    if (isEmpty(thisDayEvents)) {
      handleGetEvents();
    }
  }, []);

  const getByType = (val, type) => {
    const elm = wording[type].find((e) => e.value === val);

    return elm ? elm.label : val || '';
  };

  return (
    <WidgetContainer title="Ce jour là" padding loading={loading}>
      <Box>
        <Typography color="secondary" className={classes.title}>
          {moment().format('LL')}
        </Typography>

        <Box>
          <p>
            <b>{moment('20200620', 'YYYYMMDD').fromNow()}</b>
          </p>
          <Box mb={2}>
            <p>
              Durée: <b>{getByType(thisDayEvents.duree, 'eventDuration')}</b>
            </p>

            <p>
              Type:{' '}
              <b>{getByType(parseInt(thisDayEvents.type, 10), 'eventTypes')}</b>
            </p>
          </Box>
          <p>{thisDayEvents.short_desc}</p>
        </Box>
      </Box>
    </WidgetContainer>
  );
};

export default OnThisDay;
