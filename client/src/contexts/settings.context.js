import React, { createContext, useState } from 'react';
import PropTypes from 'prop-types';

const SettingsContext = createContext({
  values: {
    navbarTyp: 'MENU',
  },
  methods: {
    handleNavbarType: () => {},
  },
});

const SettingsProvider = ({ children }) => {
  const [navbarType, setNavbarType] = useState('MENU');

  /**
   * @param {'MENU' | 'FILTERS'} type
   */
  const handleNavbarType = (type) => {
    setNavbarType(type);
  };

  return (
    <SettingsContext.Provider
      value={{
        values: {
          navbarType,
        },
        methods: {
          handleNavbarType,
        },
      }}
    >
      {children}
    </SettingsContext.Provider>
  );
};

SettingsProvider.propTypes = {
  children: PropTypes.node,
};
export { SettingsProvider };

export default SettingsContext;
