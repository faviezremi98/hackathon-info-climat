/* eslint-disable indent */
import { createSlice } from '@reduxjs/toolkit';
import axios from 'src/utils/axios';

const initialState = {
  historicEvents: [],
};

const slice = createSlice({
  name: 'map',
  initialState,
  reducers: {
    getHistoricEventsData(state, action) {
      state.historicEventsData = action.payload;
      state.historicEventsToFilter = state.historicEventsData;
      state.overlay = false;
    },
    getHistoricEventsSearch(state, action) {
      state.historicEventsSearch = action.payload;
      state.overlay = false;
    },
    getMinAndMax(state, action) {
      state.minAndMaxDate = action.payload;
      state.overlay = false;
    },
    getOverlay(state, action) {
      state.overlay = action.payload;
    },
    getHistoricEventsDataFiltered(state, action) {
      state.historicEventsData = [
        ...state.historicEventsData,
        ...action.payload,
      ];
    },
  },
});

export const { reducer } = slice;

export const getHistoricEventsData = (params) => async (dispatch) => {
  dispatch(slice.actions.getOverlay(true));

  // eslint-disable-next-line no-console
  // console.log('params ==> ', params);

  let url = '/historic-events/by-location-data';
  if (params) {
    url += '?';
    Object.entries(params).forEach(([key, value]) => {
      value.forEach((elem) => {
        url += `${key}=${elem.value}&`;
      });
    });
  }
  const response = await axios.get(url);

  dispatch(slice.actions.getHistoricEventsData(response.data));
};

export const getHistoricEventsSearch = (params) => async (dispatch) => {
  dispatch(slice.actions.getOverlay(true));

  // eslint-disable-next-line no-console
  console.log('params ==> ', params);

  let url = '/historic-events/by-location-search';
  if (params) {
    url += '?';
    Object.entries(params).forEach(([key, value]) => {
      value.forEach((elem) => {
        url += `${key}=${elem.value}&`;
      });
    });
  }
  const response = await axios.get(url);

  dispatch(slice.actions.getHistoricEventsSearch(response.data));
};

export const resetHistoricEventsData = () => async (dispatch) => {
  dispatch(slice.actions.getHistoricEventsData([]));
};

export const resetHistoricEventsSearch = () => async (dispatch) => {
  dispatch(slice.actions.getHistoricEventsSearch([]));
};

export const getMinAndMax = (params) => async (dispatch) => {
  dispatch(slice.actions.getOverlay(true));

  // eslint-disable-next-line no-console
  // console.log('params ==> ', params);

  let url = '/historic-events/get-min-and-max-date';
  if (params) {
    url += '?';
    Object.entries(params).forEach(([key, value]) => {
      value.forEach((elem) => {
        url += `${key}=${elem.value}&`;
      });
    });
  }

  const response = await axios.get(url);

  dispatch(slice.actions.getMinAndMax(response.data));
};

export const filterEvents =
  ({ params, dateArray }) =>
  (dispatch) => {
    // console.log('params ==> ', params);
    // console.log('dateArray ==>', dateArray);
    const result = params.filter((elem) => elem.date === dateArray);
    dispatch(slice.actions.getHistoricEventsDataFiltered(result));
  };
