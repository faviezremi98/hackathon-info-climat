/* eslint-disable import/no-unresolved */
import React from 'react';
import 'date-fns';

import { BrowserRouter as Router } from 'react-router-dom';

import { StylesProvider, ThemeProvider, jssPreset } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { SnackbarProvider } from 'notistack';
import { create } from 'jss';
import renderRoutes from 'src/routes';
import routes from 'src/routes/routes';
import { createTheme } from './theme';
import GlobalStyles from './components/GlobalStyles';

const jss = create({ plugins: [...jssPreset().plugins] });

const App = () => {
  const theme = createTheme({
    theme: 'LIGHT',
  });
  return (
    <ThemeProvider theme={theme}>
      <StylesProvider jss={jss}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <SnackbarProvider dense maxSnack={3}>
            <Router>
              {renderRoutes(routes)}
              <GlobalStyles />
            </Router>
          </SnackbarProvider>
        </MuiPickersUtilsProvider>
      </StylesProvider>
    </ThemeProvider>
  );
};

export default App;
