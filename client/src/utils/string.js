const capitalize = (s) => {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
};

// eslint-disable-next-line import/prefer-default-export
export { capitalize };
