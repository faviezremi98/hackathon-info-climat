/* eslint-disable react/jsx-curly-brace-presence */
import {
  Box,
  makeStyles,
  Typography,
  Button,
  Divider,
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';

import Filters from 'src/components/map/filters';
import { useDispatch, useSelector } from 'src/store';
import TopNav from './TopNav';
import {
  getHistoricEventsSearch,
  getHistoricEventsData,
  resetHistoricEventsData,
  resetHistoricEventsSearch,
  getMinAndMax
} from '../../../../slices/map';

const useStyles = makeStyles((theme) => ({
  card: {
    padding: 0,
    boxShadow: theme.shadows[30],
    borderRadius: '10px',
    position: 'relative',
  },
  title: {
    color: theme.palette.text.primary,
    fontWeight: 600,
    fontSize: '1em!important',
  },

  customDivider: {
    width: '60px',
    marginBottom: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
    height: '4px',
  },
  pointer: {
    cursor: 'pointer',
  },
}));

const FiltersContent = ({ backToMenu }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [isValide, setIsValide] = useState(false);
  const { historicEventsSearch } = useSelector((state) => state.map);
  const [payload, setPayload] = useState({
    type: [],
    codeRegion: [],
    importance: [],
    durations: [],
  });

  useEffect(() => {
    setIsValide(
      Object.keys(payload).reduce(
        (acc, curr) => payload[curr].length > 0 || acc,
        false
      )
    );
  }, [payload]);

  const handleSetPayload = (type, obj) => {
    const currArr = payload[type];

    if (currArr.find(({ value }) => value === obj.value) !== undefined) {
      setPayload({
        ...payload,
        [type]: currArr.filter(({ value }) => value !== obj.value),
      });

      return;
    }
    setPayload({ ...payload, [type]: [...currArr, { ...obj }] });
  };

  const handleGetByLocation = () => {
    dispatch(resetHistoricEventsData());
    dispatch(resetHistoricEventsSearch());
    dispatch(getHistoricEventsSearch(payload));
  };

  const handleGetData = (id) => {
    dispatch(getHistoricEventsData({ id: [{ value: id }] }));
  };

  const handleGetMinAndMaxDate = () => {
    dispatch(getMinAndMax(payload));
  };

  const handleCall = () => {
    handleGetByLocation();
    handleGetMinAndMaxDate();
  };

  const renderCards = (elem) => (
    <div
      onClick={() => handleGetData(elem.id)}
      aria-hidden="true"
      className={classes.pointer}
    >
      <Typography className={classes.title}>
        N°{elem.id} {elem.nom}
      </Typography>
      <Divider className={classes.customDivider} />
    </div>
  );

  const reducerCards = () =>
    (historicEventsSearch || []).map((elem) => renderCards(elem));

  return (
    <Box height="100%" display="flex" flexDirection="column">
      <PerfectScrollbar options={{ suppressScrollX: true }}>
        <Box p={2} pt={3}>
          <TopNav backToMenu={backToMenu} />
          <Box mt={3}>
            <Typography className={classes.title} variant="h3" component="h3">
              Map Historic
            </Typography>
            <Box mt={2} mb={3}>
              <Typography className={classes.title} variant="h4" component="h4">
                {"Recherche d'évènement"}
              </Typography>
            </Box>
            <Filters handleSetPayload={handleSetPayload} data={payload} />

            <Box mt={2}>
              <Button
                color="secondary"
                variant="contained"
                onClick={handleCall}
                disabled={!isValide}
              >
                LANCER MA RECHERCHE
              </Button>
            </Box>
            <Box mt={2}>{reducerCards()}</Box>
          </Box>
        </Box>
      </PerfectScrollbar>
    </Box>
  );
};

FiltersContent.propTypes = {
  backToMenu: PropTypes.func,
};

export default FiltersContent;
