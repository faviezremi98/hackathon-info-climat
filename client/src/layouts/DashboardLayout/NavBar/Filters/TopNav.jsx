import React from 'react';

import { List } from '@material-ui/core';
import NavItem from '../Nav/NavItem';

import sections from '../Nav/nav-sections';

const TopNav = ({ backToMenu }) => (
  <List disablePadding>
    <NavItem {...sections[0].items[0]} onClick={() => backToMenu()} />
  </List>
);

export default TopNav;
