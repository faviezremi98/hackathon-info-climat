/* eslint-disable import/no-absolute-path */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { SettingsProvider } from 'src/contexts/settings.context';
import store from 'src/store';
import App from './App';
import reportWebVitals from './reportWebVitals';

import 'nprogress/nprogress.css';
import '@fontsource/roboto';
import 'moment/locale/fr';

import 'react-grid-layout/css/styles.css';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'react-resizable/css/styles.css';

ReactDOM.render(
  <Provider store={store}>
    <SettingsProvider>
      <App />
    </SettingsProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
