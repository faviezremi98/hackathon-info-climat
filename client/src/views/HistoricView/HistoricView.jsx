import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  Polygon,
  GeoJSON,
} from 'react-leaflet';
import { makeStyles } from '@material-ui/core/styles';

import EventDetails from 'src/components/map/EventDetails';

import { useSelector } from '../../store';
import features from '../../utils/regionData';
import TimeSlider from '../../components/map/TimeSlider';

const useStyles = makeStyles({
  root: {
    position: 'relative',
    Zindex: 1000,
    margin: '5px auto',
  },
  card: {
    position: 'absolute',
  },

  link: {
    cursor: 'pointer',
  },
});

const HistoricView = () => {
  const classes = useStyles();
  // const dispatch = useDispatch();
  const { historicEventsData, minAndMaxDate } = useSelector(
    (state) => state.map
  );

  const [markerData, setMarkerData] = useState(undefined);

  // const handleGetByLocationData = () => {
  //   dispatch(getHistoricEventsData());
  // };

  useEffect(() => {
    // handleGetByLocationData();
  }, []);

  const uniqueDay = ['2020-09-13', '2020-10-02'];
  const data = [];

  uniqueDay.forEach((day) =>
    data.push(
      (historicEventsData || []).filter((event) => event.date_deb === day)
    ));

  const renderMarkers = (elem) => (
    <Marker position={[elem.latitude, elem.longitude]}>
      <Popup>
        <Box className={classes.link} onClick={() => setMarkerData(elem)}>
          {elem.nom} à {elem.lieu}
        </Box>
      </Popup>
    </Marker>
  );

  const reducerMarkers = () =>
    (historicEventsData || []).map((elem) => renderMarkers(elem));
  const polygon = [
    [51.515, -0.09],
    [51.52, -0.1],
    [51.52, -0.12],
  ];
  return (
    <Box className={classes.root}>
      {minAndMaxDate && <TimeSlider minAndMaxDate={minAndMaxDate[0]} />}

      <EventDetails
        data={markerData}
        handleToggle={() => setMarkerData(undefined)}
      />

      <MapContainer
        style={{ height: '100vh', width: '100%' }}
        center={[47, 2]}
        zoom={6}
        scrollWheelZoom={false}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        {reducerMarkers()}
        <Polygon positions={polygon} />
        <GeoJSON data={features} />
      </MapContainer>
    </Box>
  );
};

export default HistoricView;

// TEST

// <Marker position={[48.85341, 2.3488]}>
//   <Popup>
//     <Box className={classes.link} onClick={() => setMarkerData(eventTestJson)}>
//       <a>Paris</a>
//     </Box>
//   </Popup>
// </Marker>;
