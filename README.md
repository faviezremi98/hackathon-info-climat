# Hackathon-info-climat

<div align="center">
    <img src="client/public/static/logo-info-climat.png" width="300" height="200"/>
</div>



### During this hackathon we had to give a new appearance and new ideas to the website of the association info climat, mainly on the "historic" service, which is a service of archives.


<div align="center">
    <img src="client/public/static/stack-logo.png" width="300" height="300"/>
</div>

## Requirements
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Make](https://fr.wikipedia.org/wiki/Make)


# How to setup on local environment ?



### Go to client folder and run:
```
sudo yarn install
```

### Do the same into the server folder, run:
```
sudo yarn install
```


### Now go back at source folder and launch the containers :
```
make install client
make install server

make run-local

##### Or if you don't have Make #####

docker-compose up -d 
```

### The App is launched on http://localhost:3000.

### The Server is launched on http://localhost:3050.

### You can also go to http://localhost:8000 To have access to PHPMyAdmin

## Authors

| <a href="https://gitlab.com/ivannaluzhnyi" target="_blank">**Ivan NALUZHNYI**</a> | <a href="https://gitlab.com/Dakurlz" target="_blank">**Bryan GATAY**</a> | <a href="https://gitlab.com/faviezremi98" target="_blank">**Rémi FAVIEZ** </a> | <a href="https://gitlab.com/kamISKRANE" target="_blank">**Kamel ISKRANE**</a>
| :---: |:---:|:---:| :---:|
