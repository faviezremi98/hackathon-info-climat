run start up:
	docker-compose up -d 

stop down:
	docker-compose down

install client:
	docker-compose run --rm client yarn install

install server:
	docker-compose run --rm server yarn install